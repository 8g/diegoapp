import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Pide Ahora', url: '/folder/Eat-Easy', icon: 'pizza' },
    { title: 'Mis Cupones', url: '/folder/Cupones', icon: 'trophy' },
    { title: 'Restaurantes Favoritos', url: '/folder/Restaurantes Favoritos', icon: 'heart' },
    { title: 'Mi Carrito', url: '/folder/Mi carrito de compras', icon: 'Basket' },
    { title: 'Historial De Compras', url: '/folder/Historial de compras', icon: 'Clipboard' },
    { title: 'Puntea La App', url: '/folder/Puntea nustra app!', icon: 'star' },

  ];
  objetoPersona ={
    id: 1111,
    usuario :{
      premiun: "Premiun",
      Guest: "Invitado"
    },
    name :"Diego",
    lastname :"Muñoz",
    barrio : "Av.Kennedy",
    city :"Barranquilla",
    addres :{
      street: "59",
      secund: "72",
      number: "9001"
    },
  };
  data : any;
  labels = ['Paises activos', 'Desarrolladores', 'Reportar un bug', 'Terminos y condiciones'];
  constructor() {}
}
